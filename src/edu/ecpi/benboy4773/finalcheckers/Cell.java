package edu.ecpi.benboy4773.finalcheckers;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import edu.ecpi.Checkers.CheckersConstants;
import acm.graphics.*;

public class Cell extends GRect
    implements CheckersObject, MouseListener{
	private static final long serialVersionUID = 1L;
	protected int row;
	protected int col;
	
	public Cell(int row, int col){
		//superclass constructor has to be the FIRT call in the method
		super(col * ClientConstants.GRIDDIMENSIONS/CheckersConstants.DIMENSIONS,
			  row * ClientConstants.GRIDDIMENSIONS/CheckersConstants.DIMENSIONS,
			  ClientConstants.GRIDDIMENSIONS/CheckersConstants.DIMENSIONS, 
			  ClientConstants.GRIDDIMENSIONS/CheckersConstants.DIMENSIONS
			 );
		
		this.row = row;
		this.col = col;
		decorate();
		
	}

	public void decorate() {
		if ( (col + row) % 2 == 0 ) {
			this.setFillColor(ClientConstants.BOARDCOLOR1);
		}
		else{
			this.setFillColor(ClientConstants.BOARDCOLOR2);
		}
		this.setFilled(true);
	}
	
	public int getRow(){
		return row;
	}
	
	public void setRow(int row){
		this.row = row;
	}
	
	public int getCol(){
		return col;
	}
	
	public void setCol(int col){
		this.col = col;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
