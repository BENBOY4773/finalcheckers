package edu.ecpi.benboy4773.finalcheckers;

import java.awt.Color;

public class ClientConstants {

	public static final int GRIDDIMENSIONS = 600;
	//public static final int GRIDSIZE = 8;
	public static final int PLAYERROWS = 3;
	public static final int INFOWIDTH = 17;

	public static final Color BOARDCOLOR1 = Color.decode("#8C650A");
	public static final Color BOARDCOLOR2 = Color.decode("#FAF2DE");
    
	public static final Color BLACKCOLOR = Color.BLACK;
	public static final Color REDCOLOR = Color.RED;
    
	public static final Color SELECTEDCOLOR = Color.ORANGE;
	public static final Color VALIDMOVE = Color.GREEN;
	
	//public static final int EMPTY = 0;
	//public static final int BLACK = 2;
	//public static final int BLACKKING = 6;
	//public static final int WHITE = 1;
	//public static final int WHITEKING = 5;
	
	public static final int MOVEOUTOFBOUNDS = -1;
	public static final int MOVEOCCUPIEDSELF = -2;
	public static final int MOVEOCCUPIEDOPPONENT = -3;
	public static final int MOVEOK = 1;
	
	
}
