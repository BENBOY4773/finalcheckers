package edu.ecpi.benboy4773.finalcheckers;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import edu.ecpi.Checkers.CheckersConstants;
import acm.graphics.GLabel;
import acm.graphics.GOval;


public class Checker extends GOval
		implements CheckersObject, MouseListener {
	private static final long serialVersionUID = 1L;
	protected int pieceType;
	protected int col;
	protected int row;
	protected GLabel kingLabel;
	
	
	
	public GLabel getKingLabel() {
		return kingLabel;
	}

	public void setKingLabel(GLabel kingLabel) {
		this.kingLabel = kingLabel;
	}

	public Checker(double x, double y, double width, double height ) {
		super(x, y, width, height);
	}

	public Checker(int row, int col, int pieceType) {
		//superclass constructor has to be the FIRT call in the method
		super(col * ClientConstants.GRIDDIMENSIONS/CheckersConstants.DIMENSIONS + 5,
			  row * ClientConstants.GRIDDIMENSIONS/CheckersConstants.DIMENSIONS + 5,
			  ClientConstants.GRIDDIMENSIONS/CheckersConstants.DIMENSIONS - 10, 
			  ClientConstants.GRIDDIMENSIONS/CheckersConstants.DIMENSIONS -10
			 );
		
		this.setPieceType(pieceType);
		this.setCol(col);
		this.setRow(row);
		
		this.decorate();

	}

	public void decorate() {
		switch (this.pieceType){
			case CheckersConstants.RED:
			case CheckersConstants.RED_KING:
				this.setFillColor(ClientConstants.REDCOLOR);
				break;
			case CheckersConstants.BLACK:
			case CheckersConstants.BLACK_KING:
				this.setFillColor(ClientConstants.BLACKCOLOR);
				break;
		}
		this.setFilled(true);

        if (   (this.pieceType == CheckersConstants.BLACK_KING) 
        	|| (this.pieceType == CheckersConstants.RED_KING)){
        	
        	GLabel l = new GLabel("K");
        	l.setFont("SansSerif-bold-24");
        	l.setColor(Color.YELLOW);
        	double labelX = this.getLocation().getX() + (this.getWidth() / 2) - (l.getWidth() / 2);
        	double labelY = this.getLocation().getY() + (this.getHeight() / 2) + (l.getAscent() / 2) - 3;
        	l.setLocation(labelX, labelY);
        	this.setKingLabel(l);
        	
        }
	
	}

	public int getPieceType() {
		return pieceType;
	}

	public void setPieceType(int pieceType) {
		this.pieceType = pieceType;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		Checker checker = (Checker) event.getSource();
		System.out.println("Checker is at: (" + checker.getRow() + "," + checker.getCol() + ")" );
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}