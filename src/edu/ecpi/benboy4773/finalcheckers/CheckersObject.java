package edu.ecpi.benboy4773.finalcheckers;

public interface CheckersObject {
	public int getRow();
	public int getCol();
}
