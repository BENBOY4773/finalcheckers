package edu.ecpi.benboy4773.finalcheckers;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import edu.ecpi.Checkers.CheckersConstants;
import edu.ecpi.Checkers.CheckersController;
import edu.ecpi.Checkers.CheckersGame;
import edu.ecpi.Checkers.ICheckersView;
import edu.ecpi.Checkers.Move;
import acm.graphics.*;


public class BoardUI extends JFrame implements ICheckersView, MouseListener{
	private static final long serialVersionUID = 1L;
	protected GCanvas canvas;
	protected Object model;
	protected Cell cell;
	protected BoardState boardState;
	protected CellState cellState;
	protected int gameID;
	protected boolean gameOver;
	
	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	public void setGameID(int gameID) {
		this.gameID = gameID;
	}

	public BoardUI(){
		setGameOver(false);
		boardState = new BoardState();
		cellState = new CellState(boardState); 
		cellState.initializeBoard();
		
		canvas = new GCanvas();

		this.getContentPane().add(BorderLayout.CENTER, canvas);
		canvas.setSize(ClientConstants.GRIDDIMENSIONS, ClientConstants.GRIDDIMENSIONS);
		
		//set the size of the window in which the canvas sits
		setSize(new Dimension(ClientConstants.GRIDDIMENSIONS+ClientConstants.INFOWIDTH,
							  ClientConstants.GRIDDIMENSIONS+40));
		setMinimumSize(new Dimension(ClientConstants.GRIDDIMENSIONS+ClientConstants.INFOWIDTH,
									 ClientConstants.GRIDDIMENSIONS+40));
		canvas.setVisible(true);
	}
	
	
	/**
	 * Set the board up, add cells for each play space, and attach mouse listeners.
	 * @param rows
	 * @param cols
	 * @param width
	 * @param height
	 */
	//@Override
	public void update(CheckersGame game)	{

		System.out.println("UPDATE METHOD");

		System.out.println("Received game state:");
		System.out.println(game);
		
		clearTarget();
		cellState.clearSelected();
		boardState.updateGame(game);

		System.out.println("Internal game state:");
		System.out.println(boardState.toString());

		int size = CheckersConstants.DIMENSIONS;
		for(int row = 0; row < size; row++){
			for(int col = 0; col < size; col++){
				canvas.add(cellState.cells[row][col]);
				cellState.cells[row][col].addMouseListener(this);
				if (this.boardState.getCheckerType(row, col) != CheckersConstants.EMPTY){
					canvas.add(this.boardState.getChecker(row, col));
					
					GLabel kingLabel = this.boardState.getChecker(row, col).getKingLabel();
					if (kingLabel != null){
						canvas.add(kingLabel);
					}
					
					this.boardState.getChecker(row, col).addMouseListener(this);
					cellState.cells[row][col].removeMouseListener(this);
				}
			}
		}
		this.repaint();
	}	
	
	private void clearTarget(){
		if (cellState.targetCell != null){
			cellState.targetCell.decorate();
			//cellState.getValidMoves(cellState.selectedChecker.getRow(), cellState.selectedChecker.getCol());
		}
	}

	private void setTarget(Cell c){
		cellState.targetCell = c;
		cellState.targetCell.setFillColor(ClientConstants.SELECTEDCOLOR);
	}
	
	@Override
	public void mouseClicked(MouseEvent event) {
		if (isGameOver() == true){
			return;
		}
		
		if (event.getSource() instanceof Cell){
			//System.out.println("You clicked a CELL!");
			Cell c = (Cell) event.getSource();
			if (c.getFillColor() == ClientConstants.VALIDMOVE){
				clearTarget();
				setTarget(c);
				
				///////////////////////////////////////////////////////////////
				//TODO: This needs to attach to a button click for multiple moves
				//      in one pass
				System.out.println("MAKING MOVE");
				Move move = new Move(cellState.selectedChecker.getRow(),
									 cellState.selectedChecker.getCol(),
									 cellState.targetCell.getRow(),
									 cellState.targetCell.getCol());
				
				Move[] moveArray = new Move[1];
				moveArray[0] = move;
				System.out.println("Move: " + moveArray.toString());
				CheckersController controller = CheckersController.getInstance();
				controller.getGame(gameID).updateBoard(moveArray);
                //
				////////////////////////////////////////////////////////////////
				
			}
			else {
				cellState.clearSelected();
			}
		}
		else if (event.getSource() instanceof Checker){
			//System.out.println("You clicked a CHECKER!");

			cellState.clearSelected();
			Checker c = (Checker) event.getSource();
			if (  (c.getPieceType() == CheckersConstants.BLACK)
			   || (c.getPieceType() == CheckersConstants.BLACK_KING)) {
				cellState.setSelected(c);
			}
		}
		
		//CheckersObject checkersObject = (CheckersObject) event.getSource();
		//System.out.println(checkersObject.report());

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public int getGameID() {
		//CheckersController controller = CheckersController.getInstance();
		return this.gameID;
	}

	@Override
	public void gameOver(CheckersGame game){
		setGameOver(true);
		StringBuffer sb = new StringBuffer("GAME OVER!");
		JOptionPane.showMessageDialog(null, sb, "IS510 CHeckers", JOptionPane.INFORMATION_MESSAGE);
	}

}
