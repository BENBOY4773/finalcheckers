package edu.ecpi.benboy4773.finalcheckers;

import edu.ecpi.Checkers.CheckersConstants;
import edu.ecpi.Checkers.CheckersGame;

public class BoardState {
	protected Checker[][] checkers;
	
	public BoardState(){
		checkers = new Checker[CheckersConstants.DIMENSIONS][CheckersConstants.DIMENSIONS];
	  
				for(int row = 0; row < CheckersConstants.DIMENSIONS; row++){
			for(int col = 0; col < CheckersConstants.DIMENSIONS; col++){
				
				if (((row % 2 == 0) && (col % 2 == 1))
						|| ((row % 2 == 1) && (col % 2 == 0))) {
					//First n rows black
					if  (row < ClientConstants.PLAYERROWS){
						checkers[row][col] = new Checker(row, col, CheckersConstants.BLACK);
					}
					else if ((row > CheckersConstants.DIMENSIONS - ClientConstants.PLAYERROWS -1)){
						checkers[row][col] = new Checker(row, col, CheckersConstants.RED);
					}
				}
//				if ( (row == 4) && (col == 3)){
//					//TODO: remove this block - only needed for testing valid moves
//					cells[row][col] = new Checker(row, col, CheckersConstants.BLACK);
//				}
				//We might not have created a specific checker yet, default to an empty one;
				if (checkers[row][col] == null){
					checkers[row][col] = new Checker(row, col, CheckersConstants.EMPTY);
				}
			}
			
		}
	}
	
	public void updateGame(CheckersGame game){
		int[][] board = game.getBoard();
		
		for(int row = 0; row < CheckersConstants.DIMENSIONS; row++){
			for(int col = 0; col < CheckersConstants.DIMENSIONS; col++){
				checkers[row][col].setVisible(false);
				checkers[row][col] = null;
				
				checkers[row][col] = new Checker(row, col, board[row][col]);
				
			}
		}
	}
	
	public int getCheckerType(int row, int col){
		return this.checkers[row][col].pieceType;
	}
	
	public Checker getChecker(int row, int col){
		return this.checkers[row][col];
	}

	
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();

		for(int row = 0; row < CheckersConstants.DIMENSIONS; row++){
			for(int col = 0; col < CheckersConstants.DIMENSIONS; col++){
		      	sb.append(this.getCheckerType(row, col) + " ");
			}
	      	sb.append("\n");
		}
		
		return sb.toString();
		
	}
}
