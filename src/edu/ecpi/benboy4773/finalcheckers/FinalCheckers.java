/**
 * ASSUMPTIONS:  
 * 
 * 		- Player is always black.
 * 		- No support for multiple moves in one turn, although the infrastructure is mostly there to support it.
 * 
 */
package edu.ecpi.benboy4773.finalcheckers;

import javax.swing.JOptionPane;

import edu.ecpi.Checkers.*;

public class FinalCheckers {

	public static void main(String[] args) {
		CheckersController controller = CheckersController.getInstance();
		HumanPlayer player = new HumanPlayer("Ben");
		player.setColor(CheckersConstants.BLACK);
		
		CheckersGame game = controller.createGame(player);
		
		BoardUI board = new BoardUI();
		board.setGameID(game.getGameID());
		board.update(game);
		
		board.setVisible(true);
		controller.registerView(board);
		
		StringBuffer sb = new StringBuffer("Welcome to IS510 Checkers.\n\n");
		sb.append("You play as the BLACK pieces.\n");
		sb.append("GOOD LUCK!");
		JOptionPane.showMessageDialog(null, sb, "IS510 CHeckers", JOptionPane.INFORMATION_MESSAGE);
	}

}
