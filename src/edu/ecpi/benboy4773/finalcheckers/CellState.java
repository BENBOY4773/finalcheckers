package edu.ecpi.benboy4773.finalcheckers;

import edu.ecpi.Checkers.CheckersConstants;

public class CellState {
	protected Cell[][] cells;
	protected Checker selectedChecker;
	protected Cell targetCell;
	protected Cell[] validMoves;
	protected BoardState boardState; 
	
	public CellState( BoardState boardState){
		cells = new Cell[CheckersConstants.DIMENSIONS][CheckersConstants.DIMENSIONS];
		for(int row = 0; row < CheckersConstants.DIMENSIONS; row++){
			for(int col = 0; col < CheckersConstants.DIMENSIONS; col++){
				cells[row][col] = new Cell(row, col);
			}
		}
		this.boardState = boardState;
	}

	public void clearSelected(){
		if (this.selectedChecker != null){
			this.selectedChecker.decorate(); //Restore to default state
		}
		this.targetCell = null;
        this.selectedChecker = null;
		if (this.validMoves != null){
			this.clearValidMoves();
		}
	}

	public void setSelected(Checker c){
		clearSelected();
		c.setFillColor(ClientConstants.SELECTEDCOLOR);
		this.selectedChecker = c;
		getValidMoves(c.getRow(), c.getCol(), c.getPieceType());
	}
	
	
	private int isValidMove(int row, int col){
		int result = ClientConstants.MOVEOK;
		
		//off the front or back
		if ( (row < 0) || (row > (CheckersConstants.DIMENSIONS -1))){
			return ClientConstants.MOVEOUTOFBOUNDS;
		}
		
		//off the sides
		if ( (col < 0) || (col > (CheckersConstants.DIMENSIONS -1))){
			return ClientConstants.MOVEOUTOFBOUNDS;
		}
		
		//Target cell occupied by opponent
		if (  (boardState.getChecker(row, col).pieceType == CheckersConstants.RED)
				||(boardState.getChecker(row, col).pieceType == CheckersConstants.RED_KING)){
				return ClientConstants.MOVEOCCUPIEDOPPONENT;
			}
			
		//Target cell occupied by self
		if (  (boardState.getChecker(row, col).pieceType == CheckersConstants.BLACK)
				||(boardState.getChecker(row, col).pieceType == CheckersConstants.BLACK_KING)){
				return ClientConstants.MOVEOCCUPIEDSELF;
			}

		//standard pieces can only move forward
		if ( (selectedChecker.pieceType == CheckersConstants.BLACK) && (row > selectedChecker.getRow()) ){
			return ClientConstants.MOVEOUTOFBOUNDS;
		}

		//should only now be a King with no blocking pieces
		return result;
	}
	
	
	
	public void getValidMoves(int row, int col, int pieceType){
		int move;
		int validCount = 0;
	
		//check the four corners - this is messy and needs cleaning up, but let's see how it works.
		for (int rowOffset  = -1; rowOffset < 2; rowOffset=rowOffset+2){
			for (int colOffset  = -1; colOffset < 2; colOffset=colOffset+2){
				move = isValidMove(row + rowOffset, col + colOffset); 
				if (move == ClientConstants.MOVEOK){
					cells[row + rowOffset][col + colOffset].setFillColor(ClientConstants.VALIDMOVE);
					validMoves[validCount] = cells[row + rowOffset][col + colOffset];
					validCount ++;
				} else if (move == ClientConstants.MOVEOCCUPIEDOPPONENT){
					
					if (row > 1) {
						move = isValidMove(row + (rowOffset * 2), col + (colOffset*2)); 
						if (move == ClientConstants.MOVEOK){
							cells[row + (rowOffset*2)][col + (colOffset*2)].setFillColor(ClientConstants.VALIDMOVE);
							validMoves[validCount] = cells[row + (rowOffset*2)][col + (colOffset*2)];
							validCount ++;
						}
					} else if ((row < (CheckersConstants.DIMENSIONS-1)) && (pieceType == CheckersConstants.BLACK_KING)){
						move = isValidMove(row + (rowOffset * 2), col + (colOffset*2)); 
						if (move == ClientConstants.MOVEOK){
							cells[row + (rowOffset*2)][col + (colOffset*2)].setFillColor(ClientConstants.VALIDMOVE);
							validMoves[validCount] = cells[row + (rowOffset*2)][col + (colOffset*2)];
							validCount ++;
						}
					}
				}
				
			}			
		}
	}
	
	public void clearValidMoves(){
		for (int i = 0; i < validMoves.length; i++){
			if (validMoves[i] != null){
				validMoves[i].decorate();
				validMoves[i] = null;
			}
		}
	}

	public void initializeBoard(){
		this.clearSelected();
		this.validMoves = new Cell[4];//at most four valid moves
	}

}
